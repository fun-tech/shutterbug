export interface SlideshowPosition {
	rowIndex: number;
	columnIndex: number;
}

export default SlideshowPosition;
